
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniele Claudine Muller
 */
public class Pratica41 {
    public static void main(String[] args) {
        Elipse a = new Elipse(6, 4);
        Circulo b = new Circulo(7);
        double area, perimetro;
        
        area = a.getArea();
        perimetro = a.getPerimetro();
        
        System.out.println("Area da elipse: " + area);
        System.out.println("Perimetro da elipse: " + perimetro);
        
        area = b.getArea();
        perimetro = b.getPerimetro();
        
        System.out.println("Area do circulo: " + area);
        System.out.println("Perimetro do circulo: " + perimetro);
    }
}
